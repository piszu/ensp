<?php

namespace EnspBundle\Tests\Services;

use EnspBundle\Entity\Accommodation;
use EnspBundle\Entity\Dinner;
use EnspBundle\Entity\Person;
use EnspBundle\Services\PersonManager;


class PersonManagerTest extends \PHPUnit_Framework_TestCase
{

    public function testNullPerson()
    {
        $person = $this->preparePerson();
        $this->assertEquals(0, $this->calculateCost($person));
    }

    public function preparePerson()
    {
        $acc = new Accommodation();
        $din = new Dinner();
        $person = new Person();
        $person->setAccommodation($acc);
        $person->setDinner($din);
        return $person;
    }

    public function calculateCost(Person $person)
    {
        $personManager = new PersonManager();
        return $personManager->howMuchItCost($person);
    }

    public function testOnlyTshirt()
    {
        $person = $this->preparePerson();
        $person->setShirtSize('M');
        $this->assertEquals(25, $this->calculateCost($person));
    }

    public function testAllAccommodation()
    {
        $person = $this->preparePerson();
        $this->prepareAccomodation($person, 1, 1, 1, 1, 1, 1);
        $this->assertEquals(60, $this->calculateCost($person));
    }

    public function prepareAccomodation(Person $person, $MondayTuesday, $TuesdayWednesday, $WednesdayThursday, $ThursdayFrida, $FridaySaturday, $SaturdaySundayy)
    {

        $person->getAccommodation()->setMondayTuesday($MondayTuesday);
        $person->getAccommodation()->setTuesdayWednesday($TuesdayWednesday);
        $person->getAccommodation()->setWednesdayThursday($WednesdayThursday);
        $person->getAccommodation()->setThursdayFriday($ThursdayFrida);
        $person->getAccommodation()->setFridaySaturday($FridaySaturday);
        $person->getAccommodation()->setSaturdaySunday($SaturdaySundayy);

        return $person;
    }

    public function testAllDinner()
    {
        $person = $this->preparePerson();
        $this->prepareDinner($person, 1, 1, 1, 1, 1);
        $this->assertEquals(90, $this->calculateCost($person));
    }

    public function prepareDinner(Person $person, $TuesdayDinner, $WednesdayDinner, $ThursdayDinner, $FridayDinner, $SaturdayDinner)
    {
        $person->getDinner()->setTuesdayDinner($TuesdayDinner);
        $person->getDinner()->setWednesdayDinner($WednesdayDinner);
        $person->getDinner()->setThursdayDinner($ThursdayDinner);
        $person->getDinner()->setFridayDinner($FridayDinner);
        $person->getDinner()->setSaturdayDinner($SaturdayDinner);

        return $person;
    }

    public function paramFeed()
    {
        return [
            'Test Accomodation Dinner' => ['brak', 150, [1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1]],
            'Test Tshirt Accomodation Dinner' => ['M', 165, [1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1]],
            'Test Tshirt NotFullAccomodation Dinner' => ['M', 165, [1, 1, 0, 1, 1, 1], [1, 1, 1, 1, 1]],
            'Test Tshirt Accomodation NotFUllDinner' => ['M', 157, [1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 0]],
            'Test Tshirt NotFullAccomodation NotFullDinner' => ['M', 147, [1, 1, 0, 1, 1, 1], [1, 1, 0, 1, 1]],
            'Test Tshirt NotAccomodation NotDinner' => ['L', 25, [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0]],
            'Test Tshirt Accomodation NotDinner' => ['L', 85, [1, 1, 1, 1, 1, 1], [0, 0, 0, 0, 0]],
            'Test Tshirt NotAccomodation Dinner' => ['L', 115, [0, 0, 0, 0, 0, 0], [1, 1, 1, 1, 1]],
        ];
    }

    /**
     * @dataProvider paramFeed
     */
    public function testDinnersAccomodation($tshirt, $cost, $acc, $dinner)
    {
        $person = $this->preparePerson();
        $person->setShirtSize($tshirt); //15
        $this->prepareAccomodation($person, $acc[0], $acc[1], $acc[2], $acc[3], $acc[4], $acc[5]);
        $this->prepareDinner($person, $dinner[0], $dinner[1], $dinner[2], $dinner[3], $dinner[4]);
        $this->assertEquals($cost, $this->calculateCost($person));
    }

}
