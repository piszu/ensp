<?php

namespace EnspBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PersonRepository extends EntityRepository
{
    public function isPersonExist($name, $surname)
    {
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('COUNT(p.id)')
            ->from('EnspBundle:Person', 'p')
            ->where('p.firstName = :name')
            ->andWhere("p.lastName = :surname")
            ->setParameter('name', $name)
            ->setParameter('surname', $surname)
            ->getQuery();

        $total = $query->getSingleScalarResult();

        return $total;
    }
}