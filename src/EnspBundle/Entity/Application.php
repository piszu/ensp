<?php
/**
 * Created by PhpStorm.
 * User: Piszu
 * Date: 03.02.2016
 * Time: 20:55
 */

namespace EnspBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Application
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Team")
     */
    private $team;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *      min = 0
     * )
     */
    private $count;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *      min = 0
     * )
     */
    private $totalCount;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $amountPerOne;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $totalAmount;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $advance;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;
    // TODO - PR: columns naming!
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(
     *      min = 0
     * )
     */
    private $z = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(
     *      min = 0
     * )
     */
    private $h = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(
     *      min = 0
     * )
     */
    private $hs = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(
     *      min = 0
     * )
     */
    private $w = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(
     *      min = 0
     * )
     */
    private $i = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(
     *      min = 0
     * )
     */
    private $o = 0;


    /*
     *
     * END OF FIELDS
     *
     */

    /**
     * @Assert\IsTrue(message="Liczba osób zgłoszonych musi być równa sumie uczestników w podziale na piony metodyczne.")
     */
    public function isCountLegal()
    {
        if ($this->totalCount != null) {
            return $this->totalCount == $this->z + $this->h + $this->hs + $this->w + $this->o + $this->i;
        } else {
            return $this->count == $this->z + $this->h + $this->hs + $this->w + $this->o + $this->i;
        }
    }

    /**
     * @Assert\IsTrue(message="Pamiętaj, stan liczbowy możesz tylko zwiększyć!")
     */

    public function isTotalLegal()
    {
        // for new app
        if ($this->totalCount == null) {
            return true;
        }
        /*
        if( $this->totalCount <= ceil($this->count + ($this->count*0.15))
            && $this->totalCount >= $this->count)
            {
                return true;
            }
        */
        if ($this->totalCount >= $this->count) {
            return true;
        }

        return false;
    }

    /**
     * @ORM\PrePersist
     */
    public function setPrePersistAction()
    {
        $this->modified = new \DateTime();
        $this->created = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPreUpdateAction()
    {
        $this->modified = new \DateTime();
    }


    public function __toString()
    {
        return strval($this->getTotalCount());
    }

    /*
     *
     * GETTERS AND SETTERS
     *
     */

    /**
     * @return mixed
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }

    /**
     * @param mixed $totalCount
     */
    public function setTotalCount($totalCount)
    {
        $this->totalCount = $totalCount;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param Team $team
     */
    public function setTeam(Team $team)
    {
        $this->team = $team;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getAmountPerOne()
    {
        return $this->amountPerOne;
    }

    /**
     * @param mixed $amountPerOne
     */
    public function setAmountPerOne($amountPerOne)
    {
        $this->amountPerOne = $amountPerOne;
    }

    /**
     * @return mixed
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * @param mixed $totalAmount
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;
    }

    /**
     * @return mixed
     */
    public function getAdvance()
    {
        return $this->advance;
    }

    /**
     * @param mixed $advance
     */
    public function setAdvance($advance)
    {
        $this->advance = $advance;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return mixed
     */
    public function getZ()
    {
        return $this->z;
    }

    /**
     * @param mixed $z
     */
    public function setZ($z)
    {
        $this->z = $z;
    }

    /**
     * @return mixed
     */
    public function getH()
    {
        return $this->h;
    }

    /**
     * @param mixed $h
     */
    public function setH($h)
    {
        $this->h = $h;
    }

    /**
     * @return mixed
     */
    public function getHs()
    {
        return $this->hs;
    }

    /**
     * @param mixed $hs
     */
    public function setHs($hs)
    {
        $this->hs = $hs;
    }

    /**
     * @return mixed
     */
    public function getW()
    {
        return $this->w;
    }

    /**
     * @param mixed $w
     */
    public function setW($w)
    {
        $this->w = $w;
    }

    /**
     * @return mixed
     */
    public function getI()
    {
        return $this->i;
    }

    /**
     * @param mixed $i
     */
    public function setI($i)
    {
        $this->i = $i;
    }

    /**
     * @return mixed
     */
    public function getO()
    {
        return $this->o;
    }

    /**
     * @param mixed $o
     */
    public function setO($o)
    {
        $this->o = $o;
    }

}