<?php
/**
 * Created by PhpStorm.
 * User: Piszu
 * Date: 29.01.2016
 * Time: 18:16
 */

namespace EnspBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="EnspBundle\Entity\TeamRepository")
 */
class Team
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=120)
     */
    private $teamName;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="DictEnsign")
     */
    private $ensignId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180)
     */
    private $troops;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=3)
     */
    private $isGrunwaldzka;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @ORM\Column(type="integer")
     */
    private $status = 0;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;

    /*
     *
     * END OF FIELDS
     *
     */

    public function __toString()
    {
        return strval( $this->getTeamName() );
    }

    /*
     *
     * GETTERS AND SETTERS
     *
     */

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTeamName()
    {
        return $this->teamName;
    }

    /**
     * @param string $teamName
     */
    public function setTeamName($teamName)
    {
        $this->teamName = $teamName;
    }

    /**
     * @return int
     */
    public function getEnsignId()
    {
        return $this->ensignId;
    }

    /**
     * @param int $ensignId
     */
    public function setEnsignId($ensignId)
    {
        $this->ensignId = $ensignId;
    }

    /**
     * @return string
     */
    public function getTroops()
    {
        return $this->troops;
    }

    /**
     * @param string $troops
     */
    public function setTroops($troops)
    {
        $this->troops = $troops;
    }

    /**
     * @return string
     */
    public function getIsGrunwaldzka()
    {
        return $this->isGrunwaldzka;
    }

    /**
     * @param string $isGrunwaldzka
     */
    public function setIsGrunwaldzka($isGrunwaldzka)
    {
        $this->isGrunwaldzka = $isGrunwaldzka;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }



}
