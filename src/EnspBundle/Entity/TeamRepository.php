<?php
/**
 * Created by PhpStorm.
 * User: Piszu
 * Date: 26.04.2016
 * Time: 18:23
 */

namespace EnspBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TeamRepository extends EntityRepository
{
    public function teamSummary()
    {

        $sql = "select t.id, t.teamName, t.troops, t.status, f.username, e.ensignName, a.totalCount, a.advance, a.totalAmount , sum(p.amount) payments
  from EnspBundle:Team t 
  join EnspBundle:User  f 
    with f.id = t.user
  join EnspBundle:DictEnsign e 
    with e.id = t.ensignId
  left join EnspBundle:Application a 
    with a.team = t.id
  left join EnspBundle:Payment p 
    with p.team = t.id
    group by t.id, t.teamName, t.troops, f.username, e.ensignName, a.totalCount, a.advance, a.totalAmount";

        return $this->getEntityManager()->createQuery($sql)->execute();


    }
}