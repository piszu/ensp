<?php
/**
 * Created by PhpStorm.
 * User: Piszu
 * Date: 03.02.2016
 * Time: 20:55
 */

namespace EnspBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class StatApplication
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $count;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $dateofStat;
    // TODO - PR: I;m not sure if this default value works, it should be moved to constructor
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $z = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $h = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hs = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $w = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $i = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $o = 0;
    // TODO - PR: Useless comments
    /*
     *
     * END OF FIELDS
     *
     */

    public function __toString()
    {
        return strval( $this->getTotalCount() );
    }
    // TODO - PR: Useless comments

    /*
     *
     * GETTERS AND SETTERS
     *
     */

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return \DateTime
     */
    public function getDateofStat()
    {
        return $this->dateofStat;
    }

    /**
     * @param \DateTime $dateofStat
     */
    public function setDateofStat($dateofStat)
    {
        $this->dateofStat = $dateofStat;
    }

    /**
     * @return mixed
     */
    public function getZ()
    {
        return $this->z;
    }

    /**
     * @param mixed $z
     */
    public function setZ($z)
    {
        $this->z = $z;
    }

    /**
     * @return mixed
     */
    public function getH()
    {
        return $this->h;
    }

    /**
     * @param mixed $h
     */
    public function setH($h)
    {
        $this->h = $h;
    }

    /**
     * @return mixed
     */
    public function getHs()
    {
        return $this->hs;
    }

    /**
     * @param mixed $hs
     */
    public function setHs($hs)
    {
        $this->hs = $hs;
    }

    /**
     * @return mixed
     */
    public function getW()
    {
        return $this->w;
    }

    /**
     * @param mixed $w
     */
    public function setW($w)
    {
        $this->w = $w;
    }

    /**
     * @return mixed
     */
    public function getI()
    {
        return $this->i;
    }

    /**
     * @param mixed $i
     */
    public function setI($i)
    {
        $this->i = $i;
    }

    /**
     * @return mixed
     */
    public function getO()
    {
        return $this->o;
    }

    /**
     * @param mixed $o
     */
    public function setO($o)
    {
        $this->o = $o;
    }

}