<?php

namespace EnspBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Dinner
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $tuesdayDinner = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $wednesdayDinner = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $thursdayDinner = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $fridayDinner = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $saturdayDinner = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    private $registerDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateDate;

    /**
     * @ORM\PrePersist
     */
    public function setPrePersistAction()
    {
        $this->registerDate = new \DateTime();
        $this->updateDate = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPreUpdateAction()
    {
        $this->updateDate = new \DateTime();
    }

    function __toString()
    {
        return strval($this->getId());
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTuesdayDinner()
    {
        return $this->tuesdayDinner;
    }

    /**
     * @param mixed $tuesdayDinner
     */
    public function setTuesdayDinner($tuesdayDinner)
    {
        $this->tuesdayDinner = $tuesdayDinner;
    }

    /**
     * @return mixed
     */
    public function getWednesdayDinner()
    {
        return $this->wednesdayDinner;
    }

    /**
     * @param mixed $eednesdayDinner
     */
    public function setWednesdayDinner($wednesdayDinner)
    {
        $this->wednesdayDinner = $wednesdayDinner;
    }

    /**
     * @return mixed
     */
    public function getThursdayDinner()
    {
        return $this->thursdayDinner;
    }

    /**
     * @param mixed $thursdayDinner
     */
    public function setThursdayDinner($thursdayDinner)
    {
        $this->thursdayDinner = $thursdayDinner;
    }

    /**
     * @return mixed
     */
    public function getFridayDinner()
    {
        return $this->fridayDinner;
    }

    /**
     * @param mixed $fridayDinner
     */
    public function setFridayDinner($fridayDinner)
    {
        $this->fridayDinner = $fridayDinner;
    }

    /**
     * @return mixed
     */
    public function getSaturdayDinner()
    {
        return $this->saturdayDinner;
    }

    /**
     * @param mixed $saturdayDinner
     */
    public function setSaturdayDinner($saturdayDinner)
    {
        $this->saturdayDinner = $saturdayDinner;
    }

    /**
     * @return mixed
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * @param mixed $registerDate
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;
    }

}