<?php

namespace EnspBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', NULL, array(
                'label' => 'Imię:'))
            ->add('lastName', NULL, array(
                'label' => 'Nazwisko:'))
            ->add('address', NULL, array(
                'label' => 'Adres:'))
            ->add('pesel', NULL, array(
                'label' => 'Numer PESEL:'))
            ->add('shirtSize', ChoiceType::class, array(
                'choices' => array(
                    'XS' => 'XS',
                    'S' => 'S',
                    'M' => 'M',
                    'L' => 'L',
                    'XL' => 'XL',
                    'XXL' => 'XXL'
                ),
                'label' => 'Rozmiar koszulki:'
            ))
            ->add('diet', ChoiceType::class, array(     // TODO - PR: Maybe it should be another entity? so you will be able to manage it
                'choices' => array(
                    'Zwykła' => 'Zwykła',
                    'Wegetariańska' => 'Wegetariańska',
                ),
                'label' => 'Dieta:'
            ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EnspBundle\Entity\Person',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ensp_person';
    }

}
