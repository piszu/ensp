<?php

namespace EnspBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class PaymentType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('amount', NULL, array(
                'label' => 'Kwota wpłaty:'))
            ->add('paymentDate', DateType::class, array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => 'Data wpłaty:'))
            ->add('information', NULL, array(
                'label' => 'Uwagi:'))
            ->add('save', SubmitType::class, array(
                'label' => 'Zapisz'));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'ensp_payment_edit';
    }

}
