<?php

namespace EnspBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DinnerType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('tuesdayDinner', CheckboxType::class, array(
            'label' => 'form.dinner.tuesdayDinner',
            'translation_domain' => 'EnspBundle',
            'required' => false,
        ));
        $builder->add('wednesdayDinner', CheckboxType::class, array(
            'label' => 'form.dinner.wednesdayDinner',
            'translation_domain' => 'EnspBundle',
            'required' => false,
        ));
        $builder->add('thursdayDinner', CheckboxType::class, array(
            'label' => 'form.dinner.thursdayDinner',
            'translation_domain' => 'EnspBundle',
            'required' => false,
        ));
        $builder->add('fridayDinner', CheckboxType::class, array(
            'label' => 'form.dinner.fridayDinner',
            'translation_domain' => 'EnspBundle',
            'required' => false,
        ));
        $builder->add('saturdayDinner', CheckboxType::class, array(
            'label' => 'form.dinner.saturdayDinner',
            'translation_domain' => 'EnspBundle',
            'required' => false,
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EnspBundle\Entity\Dinner',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ensp_dinner_edit';
    }

}
