<?php

namespace EnspBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class PersonStandardType extends PersonType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('save', SubmitType::class, array(
                'label' => 'Zapisz'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ensp_person_standard';
    }

}
