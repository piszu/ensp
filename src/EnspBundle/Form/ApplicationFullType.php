<?php

namespace EnspBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ApplicationFullType extends AbstractType
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;
/*
    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }
*/

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        // TODO - PR: Naming! z, h, hs ? ftw lazy bastard!
        $builder->add('count', NULL, array(
                    'label' => 'Liczba osób:'))
                ->add('z', NULL, array(
                    'label' => 'Zuchów:'))
                ->add('h', NULL, array(
                    'label' => 'Harcerzy:'))
                ->add('hs', NULL, array(
                    'label' => 'Harcerzy starszych:'))
                ->add('w', NULL, array(
                    'label' => 'Wędrowników:'))
                ->add('i', NULL, array(
                    'label' => 'Instruktorów:'))
                ->add('o', NULL, array(
                    'label' => 'Opiekunów:'))
                ->add('save', SubmitType::class, array(
                    'label' => 'Zapisz'));

    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'ensp_application_full';
    }

}
