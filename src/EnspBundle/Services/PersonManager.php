<?php

namespace EnspBundle\Services;

use EnspBundle\Entity\Person;

class PersonManager
{

    public function howMuchItCost(Person $person)
    {
        static $SINGLE_ACCOMMODATION_COST = 10.00;
        static $SINGLE_DINNER_COST = 18.00;
        static $TSHIRT_COST_PROMO = 15.00;
        static $TSHIRT_COST = 25.00;

        $amount = 0;

        $amount = $amount + ($person->getAccommodation()->getMondayTuesday() * $SINGLE_ACCOMMODATION_COST);
        $amount = $amount + ($person->getAccommodation()->getTuesdayWednesday() * $SINGLE_ACCOMMODATION_COST);
        $amount = $amount + ($person->getAccommodation()->getWednesdayThursday() * $SINGLE_ACCOMMODATION_COST);
        $amount = $amount + ($person->getAccommodation()->getThursdayFriday() * $SINGLE_ACCOMMODATION_COST);
        $amount = $amount + ($person->getAccommodation()->getFridaySaturday() * $SINGLE_ACCOMMODATION_COST);
        $amount = $amount + ($person->getAccommodation()->getSaturdaySunday() * $SINGLE_ACCOMMODATION_COST);

        $amount = $amount + ($person->getDinner()->getTuesdayDinner() * $SINGLE_DINNER_COST);
        $amount = $amount + ($person->getDinner()->getWednesdayDinner() * $SINGLE_DINNER_COST);
        $amount = $amount + ($person->getDinner()->getThursdayDinner() * $SINGLE_DINNER_COST);
        $amount = $amount + ($person->getDinner()->getFridayDinner() * $SINGLE_DINNER_COST);
        $amount = $amount + ($person->getDinner()->getSaturdayDinner() * $SINGLE_DINNER_COST);

        if ($amount >= 150.00 && $person->getshirtSize() != 'brak') {
            $amount = $amount + $TSHIRT_COST_PROMO;
        } elseif($person->getshirtSize() != 'brak') {
            $amount = $amount + $TSHIRT_COST;
        }

        return $amount;
    }









}