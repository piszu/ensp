<?php

namespace EnspBundle\Services;

use Doctrine\ORM\EntityManager;
use EnspBundle\Entity\Application;
use EnspBundle\Entity\Payment;
use EnspBundle\Entity\Team;
use Swift_Mailer;
use Twig_Environment as Environment;

class MailerManger
{
    protected $mailer;
    protected $twig;
    protected $entityManager;

    public function __construct(Swift_Mailer $mailer, Environment $twig, EntityManager $entityManager)
    {
        $this->twig = $twig;
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;

    }

    public function registerPaymentMail(Team $team, Payment $payment)
    {
        $title = 'Rejestracja płatności - Zlot Grunwaldzki 2016';
        $to = $team->getUser()->getEmail();

        $application = $this->entityManager->getRepository('EnspBundle:Application')->findOneBy(['team' => $team]);
        if ($application === null) {
            return;
        }

        $dupa = $this->twig->render('EnspBundle:Emails:pay_register_confirm.html.twig',
            array('appl' => $application,
                'payment' => $payment,
                'team' => $team)
        );
        var_dump('Przed wyslaniem');
        $this->sendMail($title, $to, $dupa);
    }

    private function sendMail($title, $to, $body)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject($title)
            ->setFrom('zlotgrunwaldzki2016@gmail.com')
            ->setTo($to)
            ->setBody($body, 'text/html');
        $this->mailer->send($message);
    }

}