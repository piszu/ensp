<?php
/**
 * Created by PhpStorm.
 * User: Piszu
 * Date: 10.12.2015
 * Time: 23:24
 */

namespace EnspBundle\Controller;

use EnspBundle\Entity\Application;
use EnspBundle\Entity\Person;
use EnspBundle\Entity\Team;
use EnspBundle\Form\PersonDutyType;
use EnspBundle\Form\PersonIndividualType;
use EnspBundle\Form\PersonGuardType;
use EnspBundle\Form\PersonStandardType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session;

class PersonController extends Controller
{

    public function EditAction(Request $request, $teamid, $personid)
    {
        $man = $this->getDoctrine()->getManager();

        /** @var Team $team */
        $team = $man->getRepository('EnspBundle:Team')->findOneBy(['id'=>$teamid]);

        if ($team->getUser() != $this->getUser()) {
            return $this->redirect($this->generateUrl('ensp_team_list'));
        }

        if ($personid == -1) {
            $person = new Person();
            $person->setTeam($team);
            $person->setPersonType('S');
        } else {
            $person = $man->getRepository('EnspBundle:Person')->find($personid);
        }

        $personForm = $this->createForm(PersonStandardType::class, $person);
        $personForm->handleRequest($request);

        if ($personForm->isSubmitted() && $personForm->isValid()) {
            $man->persist($person);
            $man->flush();

            if ($personid == -1) {
                return $this->redirect($this->generateUrl('ensp_team_view', array('teamid' => $teamid)) . '?msg=savedPerson');
            } else {
                return $this->redirect($this->generateUrl('ensp_team_view', array('teamid' => $teamid)) . '?msg=updatedPerson');
            }

        } else {
            return $this->render('EnspBundle:Ensp:person_edit.html.twig', array(
                'form' => $personForm->createView(),
                'a' => $person));
        }

    }

    public function EditGuardAction(Request $request, $teamid, $personid)
    {
        $man = $this->getDoctrine()->getManager();

        /** @var Team $team */
        $team = $man->getRepository('EnspBundle:Team')->findOneBy(['id'=>$teamid]);

        if ($team->getUser() != $this->getUser()) {
            return $this->redirect($this->generateUrl('ensp_team_list'));
        }

        if ($personid == -1) {
            $person = new Person();
            $person->setTeam($team);
            $person->setPersonType('SG');
        } else {
            $person = $man->getRepository('EnspBundle:Person')->find($personid);
        }

        $personForm = $this->createForm(PersonGuardType::class, $person);
        $personForm->handleRequest($request);

        if ($personForm->isSubmitted() && $personForm->isValid()) {
            $man->persist($person);
            $man->flush();

            if ($personid == -1) {
                return $this->redirect($this->generateUrl('ensp_team_view', array('teamid' => $teamid)) . '?msg=savedPerson');
            } else {
                return $this->redirect($this->generateUrl('ensp_team_view', array('teamid' => $teamid)) . '?msg=updatedPerson');
            }

        } else {
            return $this->render('EnspBundle:Ensp:person_edit.html.twig', array(
                'form' => $personForm->createView(),
                'a' => $person));
        }

    }

    // TODO: PR - Same fucking function in each Controller ;] Congratulations :D CopyPaste Master

    public function EditDutyAction(Request $request, $personid)
    {
        $man = $this->getDoctrine()->getManager();

        if ($personid == -1) {
            $person = new Person();
            $person->setUser($this->getUser());
            $person->setPersonType('D');
        } else {
            $person = $man->getRepository('EnspBundle:Person')->find($personid);
        }

        $personForm = $this->createForm(PersonDutyType::class, $person);
        $personForm->handleRequest($request);

        if ($personForm->isSubmitted() && $personForm->isValid()) {
            $man->persist($person);
            $man->flush();
            $this->SendEmail($person);
            return $this->redirect($this->generateUrl('ensp_duty_confirm',
                    array('teamid' => $person->getTeam()->getId(),
                        'personid' => $person->getId())) . '?msg=savedDutyPerson');
        } else {
            return $this->render('EnspBundle:Ensp:person_edit.html.twig', array(
                'form' => $personForm->createView(),
                'a' => $person,
                'teamid' => 9999));
        }
    }

    // TODO: Double functions, need to clear it!
    private function SendEmail(Person $person)
    {
        $team = $person->getTeam();
        $message = \Swift_Message::newInstance()
            ->setSubject('Rejestracja do służb - Zlot Grunwaldzki 2016')
            ->setFrom('zlotgrunwaldzki2016@gmail.com')
            ->setTo($person->getUser()->getEmail())
            ->setBody(
                $this->renderView(
                    'EnspBundle:Emails:duty_register_confirm.html.twig',
                    array('person' => $person,
                        'team' => $team)
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
    }

    public function ViewDutyAction($teamid, $personid)
    {
        $man = $this->getDoctrine()->getManager();
        $team = $man->getRepository('EnspBundle:Team')->find($teamid);
        $person = $man->getRepository('EnspBundle:Person')->find($personid);
        return $this->render('EnspBundle:Ensp:duty_confirm.html.twig', array(
            'team' => $team,
            'person' => $person
        ));
    }

    public function ViewIndividualAction($teamid, $personid)
    {
        $man = $this->getDoctrine()->getManager();
        $team = $man->getRepository('EnspBundle:Team')->find($teamid);
        $person = $man->getRepository('EnspBundle:Person')->find($personid);
        return $this->render('EnspBundle:Ensp:individual_confirm.html.twig', array(
            'team' => $team,
            'person' => $person
        ));
    }

    public function EditIndividualAction(Request $request, $personid)
    {
        $person = null;
        $man = $this->getDoctrine()->getManager();
        $team = $man->getRepository('EnspBundle:Team')->findOneBy(['teamName'=>'Instruktorzy Indywidualni']);

        if ($personid == -1) {
            $person = new Person();
            $person->setUser($this->getUser());
            $person->setTeam($team);
            $person->setPersonType('I');
        } else {
            $person = $man->getRepository('EnspBundle:Person')->find($personid);
        }

        $personForm = $this->createForm(PersonIndividualType::class, $person);
        $personForm->handleRequest($request);

        if ($personForm->isSubmitted() && $personForm->isValid()) {
            $application = $man->getRepository('EnspBundle:Application')->findOneBy(['team'=>$team]);

            $amount_to_pay = $this->get('ensp.person_manager')->howMuchItCost($person);

            $application->setTotalCount($application->getTotalCount() + 1);
            $application->setTotalAmount($application->getTotalAmount() + $amount_to_pay);
            $person->setNotes($amount_to_pay);
            $man->persist($application);
            $man->persist($person);
            $man->flush();
            $this->SendIndividualEmail($person);

            return $this->redirect($this->generateUrl('ensp_individual_confirm',
                    array('teamid' => $person->getTeam()->getId(),
                        'personid' => $person->getId())) . '?msg=savedDutyPerson');

        } else {
            return $this->render('EnspBundle:Ensp:person_individual.html.twig', array(
                'form' => $personForm->createView(),
                'a' => $person,
                'teamid' => 9999));
        }
    }

    private function SendIndividualEmail(Person $person)
    {
        $team = $person->getTeam();
        $message = \Swift_Message::newInstance()
            ->setSubject('Rejestracja Instruktor Indywidualny - Zlot Grunwaldzki 2016')
            ->setFrom('zlotgrunwaldzki2016@gmail.com')
            ->setTo($person->getUser()->getEmail())
            ->setBody(
                $this->renderView(
                    'EnspBundle:Emails:individual_register_confirm.html.twig',
                    array('person' => $person,
                        'team' => $team)
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
    }

    public function DeleteAction($teamid, $personid)
    {
        $man = $this->getDoctrine()->getManager();
        $person = $man->getRepository('EnspBundle:Person')->findOneBy(['id' => $personid]);
        $man->remove($person);
        $man->flush();
        return $this->redirect($this->generateUrl('ensp_team_view', ['teamid' => $teamid] ));
    }

}