<?php
/**
 * Created by PhpStorm.
 * User: Piszu
 * Date: 03.02.2016
 * Time: 21:21
 */

namespace EnspBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session;
use EnspBundle\Entity\Invoice;
use EnspBundle\Form\InvoiceType;

class InvoiceController extends Controller
{

    public function EditAction(Request $request, $teamid, $invid)
    {
        $invoice = null;
        $man = $this->getDoctrine()->getManager();

        $team = $man->getRepository('EnspBundle:Team')->find($teamid);

        if ($team->getUser() != $this->getUser()) {
            return $this->redirect($this->generateUrl('ensp_team_list'));
        }

        if ($invid == -1) {
            $invoice = new Invoice();
            $invoice->setCreated(new \DateTime()); // TODO: PR - Should be done in entity!
            $invoice->setTeam($team);
        } else {
            $invoice = $man->getRepository('EnspBundle:Invoice')->find($invid);
        }

        $invForm = $this->createForm(InvoiceType::class, $invoice);
        $invForm->handleRequest($request);
        // TODO: PR - Decide, Check POST or isSubmitted
        if ($request->getMethod() == 'POST') {
            if ($invForm->isSubmitted() && $invForm->isValid()) {
                $man->persist($invoice);
                $man->flush();
                return $this->redirect($this->generateUrl('ensp_team_view', array('teamid' => $teamid)) . '?msg=savedInv');
            } else {
                return $this->render('EnspBundle:Ensp:inv_edit.html.twig', array(
                    'form' => $invForm->createView(),
                    'a' => $invoice
                ));
            }
        }

        return $this->render('EnspBundle:Ensp:inv_edit.html.twig', array(
            'form' => $invForm->createView(),
            'a' => $invoice
        ));
    }
}