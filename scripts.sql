INSERT INTO ensp.dict_ensign (`id`, `ensign_name`) VALUES
(1, 'Białostocka'),
(2, 'Dolnośląska'),
(3, 'Gdańska'),
(4, 'Kielecka'),
(5, 'Krakowska'),
(6, 'Kujawsko-Pomorska'),
(7, 'Łódzka'),
(8, 'Lubelska'),
(9, 'Mazowiecka'),
(10, 'Opolska'),
(11, 'Podkarpacka'),
(12, 'Śląska'),
(13, 'Stołeczna'),
(14, 'Warmińsko-Mazurska'),
(15, 'Wielkopolska'),
(16, 'Zachodniopomorska'),
(17, 'Ziemi Lubuskiej'),
(18, 'Główna Kwatera ZHP'),
(19, 'Inne');


-- for stats
DROP EVENT `Count Statistics for application`;
CREATE DEFINER=`root`@`localhost` EVENT `Count Statistics for application` ON SCHEDULE EVERY 1 DAY STARTS '2016-02-27 00:00:00'
ON COMPLETION PRESERVE ENABLE
DO
INSERT INTO stat_application (`count`,`created`,`z`,`h`,`hs`,`w`,`i`,`o`)
SELECT SUM(`total_count`) , CURDATE() , SUM(`z`) , SUM(`h`) , SUM(`hs`) , SUM(`w`) , SUM(`i`) , SUM(`o`)
FROM `application`

    <br><b>Drużyna Kwatermistrzowska</b> - udział bezpłatny
    <br><b>HSP</b> - 100 zł
    <br><b>Recepcja</b> - 100 zł
    <br><b>Komendy Gniazd</b> - 100 zł
    <br><b>Zespół plastyka</b> - 100 zł
    <br><b>Zespół programowy</b> - 100 zł
    <br><b>Komenda Zlotu</b> - 100 zł
    <br><b>Grund Info</b> - 100 zł
    <br><b>Zespół Promocji</b> - 100 zł

-- for duty
INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'Drużyna Kwatermistrzowska', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', '0');

INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'HSP', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', '0');

INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'Recepcja', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', '0');

INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'Zespół plastyka', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', '0');

INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'Zespół programowy', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', '0');

INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'Komenda Zlotu', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', '0');

INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'Grund Info', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', '0');

INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'Zespół Promocji', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', '0');

INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'Komenda Gniazda Warmińsko-Mazurskiego', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', '0');

INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'Komenda Gniazda Środkowa Polska', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', '0');

INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'Komenda Gniazda Śląskiego', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', '0');

INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'Komenda Gniazda Mazowieckiego', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', '0');

INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'Komenda Gniazda Wielkopolskiego', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', '0');

INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'Instruktorzy Indywidualni', 'Drużyna techniczna', 'nie', '2016-04-01 00:00:00', '0');

INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'Biuro Zlotu', 'Drużyna techniczna', 'nie', '2016-04-01 00:00:00', '0');

INSERT INTO `team` ( `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`)
VALUES ( '14', '1', 'Biuro WDG', 'Drużyna techniczna', 'nie', '2016-04-01 00:00:00', '0');



/*
PROD:

parameters:
    database_host: 127.0.0.1
    database_port: null
    database_name: zg2016
    database_user: zg2016
    database_password: VeWMfct92ZzwGTBC
    mailer_transport: gmail
    mailer_host:
    mailer_user: zlotgrunwaldzki2016
    mailer_password: ZAQ!2wsx02
    secret: ThisTokenIsNotSoSecretChangeIt

 */


/*
Select dla Moniki:

SELECT `team`.id as TEAM_ID,
	     `team_name`as Drużyna,
       `dict_ensign`.`ensign_name` as Choragiew,
       team.troops as Hufiec,
       fos_user.username as user,
       fos_user.name ,
       fos_user.surname,
       fos_user.phone,
       fos_user.email,
       application.total_count as zglosoznych
  FROM `team`,
  	   `application`,
       `fos_user`,
       `dict_ensign`
 WHERE `team`.`id`= `application`.`team_id`
   AND `fos_user`.`id` = `team`.`user_id`
   AND `dict_ensign`.`id` = `team`.`ensign_id_id`
   order by team.id ASC

Zgłoszenia do służb:

Zapytanie:
SELECT `person`.`id`,
`team`.`team_name`,
`person`.`first_name`,
`person`.`last_name`,
`person`.`address`,
`person`.`pesel`,
`person`.`shirt_size`,
`person`.`diet`,
`fos_user`.`username`,
`fos_user`.`email`,
`fos_user`.`phone`,
`fos_user`.`info`
FROM `person`
JOIN `team` ON `team`.`id` = `person`.`team_id`
JOIN `fos_user` ON `fos_user`.`id` = `person`.`user_id`
WHERE `person_type` = 'D';
*/
